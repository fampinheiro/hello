const server = require('./server')

server.init(handleErr((server) => {
  server.start(handleErr(serverStarted))

  function serverStarted () {
    server.log(['start'], `server running on port ${server.info.port}`)
  }
}))

function handleErr (cb) {
  return (err, server) => {
    if (err) {
      throw err
    }
    return cb(server)
  }
}
