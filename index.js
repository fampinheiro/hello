module.exports = function register (server, options, next) {
  server.route({
    handler: function (request, reply) {
      return reply({
        now: new Date().toISOString()
      })
    },
    method: 'get',
    path: '/'
  })
  return next()
}

module.exports.attributes = {
  pkg: require('./package.json')
}
