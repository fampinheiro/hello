FROM node:4.4.2-slim

WORKDIR /usr/src/app

ADD package.json package.json
RUN ["npm", "install"]
ADD . .

CMD ["npm","start"]
