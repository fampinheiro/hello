const Hapi = require('hapi')
const series = require('run-series')
const waterfall = require('run-waterfall')

const PLUGINS = [{
  register: {
    register: require('good'),
    options: {
      opsInterval: 1000,
      reporters: [{
        reporter: require('good-console'),
        events: {
          log: '*',
          ops: '*',
          response: '*'
        }
      }]
    }
  }
}, {
  register: {
    register: require('./')
  }
}]

exports.init = function (cb) {
  return waterfall([
    createServer,
    registerPlugins
  ], cb)
}

function createServer (cb) {
  const server = new Hapi.Server({
    connections: {
      routes: {
        state: {
          parse: false,
          failAction: 'ignore'
        }
      }
    }
  })
  server.connection({ port: 3000 })
  return cb(null, server)
}

function registerPlugins (server, cb) {
  return series(PLUGINS.map(registerPlugin), (err) => {
    return cb(err, server)
  })
  function registerPlugin (plugin, cb) {
    return (cb) => {
      const options = plugin.options || {}
      server.register(plugin.register, options, cb)
    }
  }
}
